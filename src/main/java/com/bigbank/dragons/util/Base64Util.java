package com.bigbank.dragons.util;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

public class Base64Util {
    public static String safeDecode(String input) {
        try {
            byte[] decode = Base64.getDecoder().decode(input);
            return new String(decode, StandardCharsets.UTF_8);
        } catch (Exception e) {
            return null;
        }
    }
}
