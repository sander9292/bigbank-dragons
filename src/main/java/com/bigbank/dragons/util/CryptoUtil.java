package com.bigbank.dragons.util;

import org.apache.commons.lang3.StringUtils;

public class CryptoUtil {
    public static String decrypt(String input) {
        String result = Base64Util.safeDecode(input);
        if (StringUtils.isAlphanumericSpace(result)) {
            return result;
        }
        return null;
    }
}
