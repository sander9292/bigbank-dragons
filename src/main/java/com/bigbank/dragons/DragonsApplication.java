package com.bigbank.dragons;

import com.bigbank.dragons.algorithm.PlayerHealingAwareGameSolver;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class DragonsApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(DragonsApplication.class, args);

        PlayerHealingAwareGameSolver solver = context.getBean(PlayerHealingAwareGameSolver.class);
        solver.start();

        context.close();
    }
}
