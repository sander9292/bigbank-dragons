package com.bigbank.dragons.model;

import lombok.Data;

@Data
public class ShopAttemptOutcome {
    private Boolean shoppingSuccess;
    private Integer gold;
    private Integer lives;
    private Integer level;
    private Integer turn;
}
