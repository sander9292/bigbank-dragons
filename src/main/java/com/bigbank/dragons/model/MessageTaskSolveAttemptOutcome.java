package com.bigbank.dragons.model;

import lombok.Data;

@Data
public class MessageTaskSolveAttemptOutcome {
    private Boolean success;
    private Integer lives;
    private Integer gold;
    private Integer score;
    private Integer highScore;
    private Integer turn;
    private String message;
}
