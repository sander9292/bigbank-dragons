package com.bigbank.dragons.model;

import com.bigbank.dragons.util.CryptoUtil;
import lombok.Data;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

@Data
public class MessageTask {
    private String adId;
    private String message;
    private BigDecimal reward;
    private Integer expiresIn;
    private Boolean encrypted;
    private String probability;

    public boolean isUnencryptable() {
        return StringUtils.isNotBlank(getUnencryptedAdId()) && StringUtils.isNotBlank(getUnencryptedProbability());
    }

    public String getUnencryptedProbability() {
        if (BooleanUtils.isNotTrue(getEncrypted())) {
            return getProbability();
        }
        return CryptoUtil.decrypt(getProbability());
    }

    public String getUnencryptedAdId() {
        if (BooleanUtils.isNotTrue(getEncrypted())) {
            return getAdId();
        }
        return CryptoUtil.decrypt(getAdId());
    }
}
