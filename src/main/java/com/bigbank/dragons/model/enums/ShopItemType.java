package com.bigbank.dragons.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ShopItemType {
    HEALING_POTION("Healing potion"),
    MEGA_TRICKS("Book of Megatricks");

    private final String textValue;
}
