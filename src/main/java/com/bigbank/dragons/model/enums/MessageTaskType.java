package com.bigbank.dragons.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum MessageTaskType {
    STEAL("Steal");

    private String namePrefix;

    public static MessageTaskType getByTaskName(String taskName) {
        return Arrays.stream(MessageTaskType.values()).filter(type -> StringUtils.startsWith(taskName, type.getNamePrefix())).findFirst().orElse(null);
    }
}
