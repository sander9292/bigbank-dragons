package com.bigbank.dragons.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Objects;

@AllArgsConstructor
@Getter
public enum Probability {
    IMPOSSIBLE("Impossible", BigDecimal.valueOf(0.0)),
    SUICIDE_MISSION("Suicide mission", BigDecimal.valueOf(0.05)),
    RISKY("Risky", BigDecimal.valueOf(0.1)),
    QUITE_LIKELY("Quite likely", BigDecimal.valueOf(0.5)),
    WALK_IN_PARK("Walk in the park", BigDecimal.valueOf(0.6)),
    PIECE_OF_CAKE("Piece of cake", BigDecimal.valueOf(0.7)),
    SURE_THING("Sure thing", BigDecimal.valueOf(1.0));

    private final String textValue;
    private final BigDecimal probabilityEstimate;

    public static Probability getByTextValue(String textValue) {
        return Arrays.stream(Probability.values()).filter(p -> Objects.equals(p.getTextValue(), textValue)).findFirst().orElse(null);
    }

}
