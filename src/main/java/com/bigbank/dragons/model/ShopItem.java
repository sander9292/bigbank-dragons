package com.bigbank.dragons.model;

import lombok.Data;

@Data
public class ShopItem {
    private String id;
    private String name;
    private Integer cost;
}
