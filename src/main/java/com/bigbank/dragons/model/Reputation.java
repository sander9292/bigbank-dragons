package com.bigbank.dragons.model;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class Reputation {
    private BigDecimal people;
    private BigDecimal state;
    private BigDecimal underworld;
}
