package com.bigbank.dragons.algorithm;

import com.bigbank.dragons.model.Game;
import com.bigbank.dragons.model.MessageTask;
import com.bigbank.dragons.model.ShopItem;
import com.bigbank.dragons.model.MessageTaskSolveAttemptOutcome;
import com.bigbank.dragons.model.enums.ShopItemType;
import com.bigbank.dragons.service.DragonApiService;
import com.bigbank.dragons.service.MessageQualityEvaluationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Slf4j
@AllArgsConstructor
public class PlayerHealingAwareGameSolver {

    private DragonApiService dragonApiService;
    private MessageQualityEvaluationService messageQualityEvaluationService;

    public void start() {
        Game game = dragonApiService.startGame();
        log.debug("Started game");
        log.debug("Game info: {}", game);

        int livesRemaining = game.getLives();
        while (livesRemaining > 0) {
            MessageTask maxMessage = messageQualityEvaluationService.findHighestQualityMessage(game);
            if (maxMessage == null) {
                log.debug("No consumable message, stopping");
                break;
            }
            log.debug("Solving message: {}", maxMessage);
            MessageTaskSolveAttemptOutcome outcome = dragonApiService.solve(game.getGameId(), maxMessage.getUnencryptedAdId());
            log.debug("Solve attempt outcome: {}", outcome);
            livesRemaining = outcome.getLives();

            if (livesRemaining == 1) {
                buyHealth(game, outcome.getGold());
            }
            log.debug("Current state - Score: {}, livesRemaining: {}", outcome.getScore(), livesRemaining);
        }
        log.debug("Game Over");
    }

    private void buyHealth(Game game, int goldRemaining) {
        List<ShopItem> shopItems = dragonApiService.getShopItems(game.getGameId());

        Optional<ShopItem> healPotion = shopItems.stream().filter(item -> item.getCost() < goldRemaining && ShopItemType.HEALING_POTION.getTextValue().equals(item.getName())).findFirst();
        healPotion.ifPresent(shopItem -> dragonApiService.purchaseShopItem(game.getGameId(), shopItem.getId()));
    }
}
