package com.bigbank.dragons.service;

import com.bigbank.dragons.model.Game;
import com.bigbank.dragons.model.MessageTask;
import com.bigbank.dragons.model.MessageTaskSolveAttemptOutcome;
import com.bigbank.dragons.model.Reputation;
import com.bigbank.dragons.model.ShopAttemptOutcome;
import com.bigbank.dragons.model.ShopItem;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class DragonApiService {
    private final RestTemplate restTemplate;

    @Setter
    @Value("${dragon.api.baseurl}")
    private String apiBaseUrl;

    public Game startGame() {
        return restTemplate.postForObject(apiBaseUrl + "/api/v2/game/start", null, Game.class);
    }

    public List<MessageTask> getAllMessages(String gameId) {
        MessageTask[] messages = restTemplate.getForObject(apiBaseUrl + "/api/v2/" + gameId + "/messages", MessageTask[].class);
        return Arrays.asList(messages);
    }

    public MessageTaskSolveAttemptOutcome solve(String gameId, String adId) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("gameId", gameId);
        uriVariables.put("adId", adId);
        return restTemplate.postForObject(apiBaseUrl + "/api/v2/{gameId}/solve/{adId}", null, MessageTaskSolveAttemptOutcome.class, uriVariables);
    }

    public List<ShopItem> getShopItems(String gameId) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("gameId", gameId);
        ShopItem[] items = restTemplate.getForObject(apiBaseUrl + "/api/v2/{gameId}/shop", ShopItem[].class, uriVariables);
        return Arrays.asList(items);
    }

    public ShopAttemptOutcome purchaseShopItem(String gameId, String shopItemId) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("gameId", gameId);
        uriVariables.put("shopItemId", shopItemId);
        return restTemplate.postForObject(apiBaseUrl + "/api/v2/{gameId}/shop/buy/{shopItemId}", null, ShopAttemptOutcome.class, uriVariables);
    }

    public Reputation getReputation(String gameId) {
        Map<String, String> uriVariables = new HashMap<>();
        uriVariables.put("gameId", gameId);
        return restTemplate.postForObject(apiBaseUrl + "/api/v2/{gameId}/investigate/reputation", null, Reputation.class, uriVariables);
    }
}
