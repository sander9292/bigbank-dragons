package com.bigbank.dragons.service;

import com.bigbank.dragons.model.Game;
import com.bigbank.dragons.model.MessageTask;
import com.bigbank.dragons.model.Reputation;
import com.bigbank.dragons.model.enums.MessageTaskType;
import com.bigbank.dragons.model.enums.Probability;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;

@Service
@Slf4j
public class MessageQualityEvaluationService {

    private final DragonApiService dragonApiService;
    private final ReputationAwareMessageTaskDangerEvaluator messageTaskDangerEvaluator;

    @Autowired
    public MessageQualityEvaluationService(DragonApiService dragonApiService) {
        this.dragonApiService = dragonApiService;
        this.messageTaskDangerEvaluator = new ReputationAwareMessageTaskDangerEvaluator();
    }

    public MessageTask findHighestQualityMessage(Game game) {
        Reputation userReputation = dragonApiService.getReputation(game.getGameId());
        List<MessageTask> messages = dragonApiService.getAllMessages(game.getGameId());

        return messages.stream().max(
                Comparator.comparing((MessageTask msg) -> getMessageExpectedRewardEstimate(msg, userReputation))
                        .thenComparing(MessageTask::getExpiresIn, Comparator.reverseOrder())
        ).orElse(null);
    }

    private BigDecimal getMessageExpectedRewardEstimate(MessageTask msg, Reputation userReputation) {
        if (!msg.isUnencryptable()) {
            return BigDecimal.ZERO;
        }
        Probability probability = getSuccessProbability(msg);
        BigDecimal estimate = msg.getReward().multiply(probability.getProbabilityEstimate());
        if (messageTaskDangerEvaluator.isDangerous(msg, userReputation)) {
            estimate = estimate.divide(BigDecimal.TEN, RoundingMode.HALF_UP);
        }
        return estimate;
    }

    private Probability getSuccessProbability(MessageTask msg) {
        Probability probability = Probability.getByTextValue(msg.getUnencryptedProbability());
        if (probability == null) {
            probability = Probability.SUICIDE_MISSION; // assume it is bad
        }
        return probability;
    }

    private static class ReputationAwareMessageTaskDangerEvaluator {
        private static final BigDecimal MIN_SAFE_STATE_REPUTATION = BigDecimal.valueOf(-6);

        public boolean isDangerous(MessageTask msg, Reputation userReputation) {
            boolean isStealingTaskType = MessageTaskType.getByTaskName(msg.getMessage()) == MessageTaskType.STEAL;
            boolean hasLowReputation = userReputation.getState().compareTo(MIN_SAFE_STATE_REPUTATION) <= 0;
            return isStealingTaskType && hasLowReputation;
        }
    }
}
