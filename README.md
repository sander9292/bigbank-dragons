# bigbank-dragons

Solution for backend task of Dragons of Mugloar (www.dragonsofmugloar.com)

Running instructions:  
`gradlew.bat bootRun` (Win)  
`gradlew bootRun` (Linux)  
or import to IDE as Spring Boot/Gradle project  
